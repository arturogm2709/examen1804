const express = require('express')
const router = express.Router()
const materiaController = require('../controllers/materiaController.js')
const auth = require('../middlewares/auth')
const servicejwt = require('../services/servicejwt')

const moment = require('moment')

router.get('/', (req, res) => {
  // console.log('hemos pasado')
  materiaController.index(req, res)
})
router.get('/privada', auth.auth, materiaController.privada)

router.get('/:id', (req, res) => {
  // console.log('hemos pasado')
  materiaController.show(req, res)
})
router.post('/', (req, res) => {
  materiaController.create(req, res)
})
router.put('/:id', (req, res) => {
  materiaController.update(req, res)
})
router.delete('/:id', (req, res) => {
  materiaController.remove(req, res)
})

module.exports = router
