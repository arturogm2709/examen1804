const express = require('express')
const router = express.Router()
const userController = require('../controllers/userController')

// registro de un nuevo usuario
router.post('/', userController.register)

module.exports = router
