const express = require('express')
const router = express.Router()
const routerMaterias = require('./routes/materias.js')
const routerUsers = require('./routes/users.js')

router.get('/', (req, res) => {
  res.json({ mensaje: '¡Bienvenido a nuestra API con MongoDB!' })
})

router.use('/materias', routerMaterias)
router.use('/users', routerUsers)

module.exports = router
