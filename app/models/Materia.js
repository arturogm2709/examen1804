const mongoose = require('mongoose')
const Schema = mongoose.Schema

const materiaSchema = new Schema({
  codigo: {
    type: String,
    required: true
  },
  nombre: String,
  curso: String,
  horas: String
})

const Materia = mongoose.model('Materia', materiaSchema)

module.exports = Materia
