const mongoose = require('mongoose')
const Schema = mongoose.Schema
const UserSchema = new Schema({
  codigo: String,
  password: String,
  nombre: String
})

module.exports = mongoose.model('User', UserSchema)
